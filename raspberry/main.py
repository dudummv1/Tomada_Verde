import plug_power
import time
import RPi.GPIO as GPIO

def check_power(pp):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(7,GPIO.OUT)
    pp.check_hour()
    GPIO.output(7,pp.state)
    while True:
        time.sleep(300)
        if pp.check_hour():
            GPIO.output(7,not GPIO.input(7))


plug = plug_power.plug_power()
check_power(plug)
d